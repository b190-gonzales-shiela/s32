const http=require("http");

const port=4000;



const server=http.createServer(function(request,response){


	if (request.url==="/" && request.method === "GET") {
		response.writeHead(200,{"Content-Type":"text/plain"});
		response.end("Welcome to the booking system");

	} if (request.url==="/profile" && request.method === "GET") {
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end("Welcome to your profile");

	} if(request.url==="/courses" && request.method === "GET") {
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end("Here's the available courses");


	} if(request.url==="/addcourses" && request.method === "PUT") {
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end("Add a course");

	} if (request.url==="/updatecourse" && request.method === "POST"){
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end("Update a course to our resources");
	} else (request.url==="/archivecourse" && request.method === "DELETE")
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Course added to archives");
	

	});

server.listen(port);

console.log(`Server now running at port: ${port}`);